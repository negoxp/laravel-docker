#!/bin/bash
set -e

### Configuration ###
## INSERT YOUR IPS HERE WITH SPACES BETWEEN
IP_ADDRESSES=(52.13.26.189)

## VARS ##

BRANCH=$(git rev-parse --abbrev-ref HEAD)

## Deploy Script ##

if [[ "$BRANCH" != "master" ]]; then
  echo ‘ABORTING DEPLOY: Not on master branch’;
  exit 0;
fi

docker-compose push
git push

for ip in ${IP_ADDRESSES[@]}; do
    cd ~
    ssh -i '~/cw-sports.pem' ec2-user@${ip} ‘
        cd laravel-docker \
        && sudo git pull \
        && docker-compose pull && docker-compose up –-build \
        && docker exec laravel-docker_app_1 composer update \
        && docker exec laravel-docker_app_1 php artisan route:clear \
        && docker exec laravel-docker_app_1 php artisan config:clear \
        && docker exec laravel-docker_app_1 php artisan cache:clear \
    ‘

done
exit 0
